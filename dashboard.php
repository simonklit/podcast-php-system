<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>
Podcast Dashboard
</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
</head>
<body>
<div>
<div class="container">
  <h1>Podcast Dashboard</h1>
  <?php 
  if (isset($_SESSION['xmlcomplete'])) {
   echo '<div class="alert alert-success">'.$_SESSION['xmlcomplete'].'</div>';
   unset($_SESSION['xmlcomplete']);
  }
  if (isset($_SESSION['jsoncomplete'])) {
   echo '<div class="alert alert-success">'.$_SESSION['jsoncomplete'].'</div>';
   unset($_SESSION['jsoncomplete']);
  }
  if(!isset($_SESSION['loggedin'])) {
  	?>
  <form action="" method="post"><div class="row">
  <div class="col-lg-4">
    <div class="input-group">
		<span class="input-group-addon">Password</span>
      <input type="password" name="password" class="form-control">
      <span class="input-group-btn">
        <button type="submit" class="btn btn-default" type="button">Log in</button>
      </span>
    </div>
  </div>
  </div></form> <?php
  }else{
  }
  if($_POST['password'] == "ukpodpass") {
    $_SESSION['loggedin'] = "yes";
    echo "<script>window.location = 'dashboard.php'</script>";
  }
  if ($_SESSION['loggedin'] == "yes") {
  ?>
<a href="add.php" class="btn btn-info">Tilføj</a> <a href="edit.php" class="btn btn-info">Rediger</a> <a href="xml.php" class="btn btn-default">Generér XML fra JSON</a>
<br><br>
<div class="row">
  <div class="col-lg-4">
  <div class="well well-sm">
Filer: <a href="podcast.json" target="_blank">podcast.json</a> <a href="rss.xml" target="_blank">rss.xml</a>
</div>
</div></div>

<?php }?>
</div>
</div>
</body>
</html>