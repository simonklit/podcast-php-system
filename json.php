<?php
//Start session so that we can save information to session
session_start();

//Library for encoding json with indents on older PHP-versions
require_once('nicejson.php');

//If all information is passed
if(isset ($_POST['taler'], $_POST['date'])) {

//Set user information in variables
$taler = htmlspecialchars($_POST['taler']);

$date = date("Y-m-d", strtotime($_POST['date'])); //Ensures that date is in the correct format

//Load current json file and decode it
$input = json_decode(file_get_contents("podcast.json"), true);

//Push new information to the end of the array decoded from json
array_push($input, Array(taler => $taler, date => $date));

//Encode information and place in output variable
$output = json_format($input);

//Display it on page (not required by any means)
//echo "<pre>" . $output . "</pre>";

//Write information to podcast.json file
$fp = fopen('podcast.json', 'w');
fwrite($fp, $output);
fclose($fp);

//Script complete, store message to session
$_SESSION['jsoncomplete'] = 'Taleren er blevet tilføjet, og JSON er blevet genereret og gemt som "<a href="podcast.json" target="_blank" class="alert-link">podcast.json</a>"';
}

//Redirect to generation when script is complete
echo "<script>window.location = 'dashboard.php'</script>";
?>