<?php
//Start session so that we can save information to session
session_start();

//Load and decode json information
$jsoninput = json_decode(file_get_contents("podcast.json"), true);

//Set static information to be prepended always in variable
 $prepend = '<?xml version="1.0" encoding="utf-8"?> <rss version="2.0" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"> <channel> <title>Taler fra Grande</title> <itunes:category text="Religion &amp; Spirituality"> <itunes:category text="Christianity" /> </itunes:category> <itunes:image href="http://ungkirke.com/podcast/cover.jpg"></itunes:image> <itunes:author>UngKirke</itunes:author> <itunes:explicit>no</itunes:explicit> <link>http://ungkirke.com/</link> <description>UngKirke holder Grande ungdomsgudstjeneste hver anden fredag kl 20:00 i Aalborg Citykirke</description> <language>da</language> <copyright>UngKirke</copyright>';

//Run loop through each item in decoded json, generating xml from results
foreach ($jsoninput as $value){
$middle .= '<item>
<title>' . $value[taler] . ' - Grande ' . $value[date] . '</title>
<description>' . $value[taler] . ' - Grande ' . $value[date] . '</description>
<link>http://ungkirke.com/podcast/files/grande_' . $value[date] . '.mp3</link>
<itunes:author>UngKirke</itunes:author>
<itunes:explicit>no</itunes:explicit>
<pubDate>'. date("D, d M Y", strtotime($value[date])) .' 20:00:00 +0100</pubDate>
<enclosure url="http://ungkirke.com/podcast/files/grande_' . $value[date] . '.mp3" type="audio/mpeg" />
<guid>http://ungkirke.com/podcast/files/grande_' . $value[date] . '</guid>
</item>';
}

//Set static information to be appended always in variable
$append = "</channel></rss>";

//Concatenate all information in output variable
$output = $prepend . $middle . $append;

//Create rss.xml file with final information
$fp = fopen('rss.xml', 'w');
fwrite($fp, $output);
fclose($fp);

//Script complete, store message to session
$_SESSION['xmlcomplete'] = 'XML er blevet genereret og gemt som "<a href="rss.xml" class="alert-link" target="_blank">rss.xml</a>".';

//Redirect back to generation page
echo "<script>window.location = 'dashboard.php'</script>";
?>
