<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>
Rediger talere
</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
<h1>Rediger talere</h1>
<?php
if ($_SESSION['loggedin'] == "yes") {
//Library for encoding json with indents on older PHP-versions
require_once('nicejson.php');

//Load current json file and decode it
$input = json_decode(file_get_contents("podcast.json"), true);

//Display delete message if it exists
if (isset($_SESSION['deletecomplete'])) {
	echo '<div class="alert alert-success">'.$_SESSION['deletecomplete'].'</div>';
	unset($_SESSION['deletecomplete']);
}

//If user information is set (if an edit has been submitted)
if (isset($_POST['taler'],$_POST['date'])) {
	$id = $_POST['id'];

	//Set array information to the new-given user information
	$input[$id][taler] = htmlspecialchars($_POST['taler']);
	$input[$id][date] = date("Y-m-d", strtotime($_POST['date']));
	
	//Encode information and place in output variable
	$output = json_format($input);

	//Write new information to podcast.json file
	$fp = fopen('podcast.json', 'w');
	fwrite($fp, $output);
	fclose($fp);
	?>
	<div class="alert alert-success"><?php echo 'Taleren: '.$_POST['oldtaler'].' er blevet ændret til: '.$_POST['taler'];?><br><?php echo 'Datoen: '.$_POST['olddate'].' er blevet ændret til: '.$_POST['date'];?><br>Husk at <a href="xml.php">generere XML fra JSON</a>, da kun JSON'en er blevet opdateret.</div>
	<a class="btn btn-default" href="dashboard.php">Tilbage til Dashboard</a> <a class="btn btn-default" href="edit.php">Tilbage til redigeringsmenuen</a>
	<?php
}else{
//If id has not been set as a get variable (blank slate)
if (!isset($_GET['id'])) {
?>
<div class="row">  <div class="col-lg-4">
<a class="btn btn-default" href="dashboard.php">Tilbage</a><br><br></div></div>
<?php
//Iterate over each entry from the decoded json, displaying the results
$i = -1;
foreach ($input as $value){
	$i++;
	echo "<div class='well well-sml'>Taler: ". $value[taler]. "<br>";
	echo "Dato: ". date("d M Y", strtotime($value[date]))." (".$value[date].")<br>";
	echo '<a class="btn btn-primary" href="edit.php?id='. $i . '">Rediger</a>';
	echo ' <a class="btn btn-danger" href="delete.php?id='. $i . '" onclick="return confirmAction()">Slet</a></div>';
}
//If an id has been set as a get variable (after "rediger" has been clicked)
}else{
	$id = $_GET['id'];

	//Create form and paste information in the fields
	?>
	<a class="btn btn-default" href="edit.php">Tilbage</a><br><br>
	<form action="" method="post">
	<div class="row">
	  <div class="col-lg-3">
    <div class="input-group">
		<span class="input-group-addon">Taler</span>
      <input type="text" name="taler" value="<?php print_r($input[$id][taler])?>" class="form-control">
    </div>

  </div>
  <div class="col-lg-3">
    <div class="input-group">
    		<span class="input-group-addon">Dato (YYYY-mm-dd)</span>
      <input type="text" name="date" value="<?php print_r($input[$id][date])?>" class="form-control">
    </div>
  </div>
    <div class="col-lg-1">
    <input type="hidden" name="id" value="<?php echo $id; ?>">
    <input type="hidden" name="oldtaler" value="<?php print_r($input[$id][taler])?>">
    <input type="hidden" name="olddate" value="<?php print_r($input[$id][date])?>">
	<button type="submit" class="btn btn-success"><span>Rediger</span></button></form>
	</div>
  </div>
	<?php
}
}
}else{
	echo "<script>window.location = 'dashboard.php'</script>";
}
?>
</div>
<script>function confirmAction(){
      var confirmed = confirm("Vil du virkelig slette?");
      return confirmed;
}</script>
</body>
</html>