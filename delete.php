<?php
session_start();

//Is logged in?
if ($_SESSION['loggedin'] == "yes") {

//If an id is provided
if (isset($_GET['id'])) {

//Library for encoding json with indents on older PHP-versions
require_once('nicejson.php');

//Set current id in variable
$id = $_GET['id'];

//Load current json file and decode it
$input = json_decode(file_get_contents("podcast.json"), true);

//Put speaker and date for selected entry in variables for complete message
$taler = $input[$id][taler];
$date = $input[$id][date];

//Remove the selected id from the array
unset($input[$id]);

//Fix integers, so that we don't have integer skips
$input = array_values($input);

//Encode information and place in output variable
$output = json_format($input);

//Write new information to podcast.json file
$fp = fopen('podcast.json', 'w');
fwrite($fp, $output);
fclose($fp);

//Set complete message to session
$_SESSION['deletecomplete'] = "Følgende taler er nu blevet slettet: ".$taler.", og den havde følgende dato: ".$date;

//Redirect to edit page
echo "<script>window.location = 'edit.php'</script>";
}
}else{
	echo "<script>window.location = 'dashboard.php'</script>";
}
?>