<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>
Tilføj taler
</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
</head>
<body>
<div>
<div class="container">
  <h1>Tilføj taler</h1>
  <div class="row">  <div class="col-lg-4">
<a class="btn btn-default" href="dashboard.php">Tilbage</a><br><br></div></div>
  <?php 
  if(!isset($_SESSION['loggedin'])) {
  //Redirect to xml-generation when script is complete
  echo "<script>window.location = 'dashboard.php'</script>";
  }else{
  if($_SESSION['loggedin'] == "yes") {
  ?>
  <form action="json.php" method="post">
  <div class="row">
  <div class="col-lg-4">
    <div class="input-group">
		<span class="input-group-addon">Taler</span>
      <input type="text" name="taler" class="form-control">
    </div>
  </div>
  <div class="col-lg-4">
    <div class="input-group">
    		<span class="input-group-addon">Dato (YYYY-mm-dd)</span>
      <input type="text" name="date" class="form-control">
    </div>
  </div>
  <div class="col-lg-4">
<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span><span>  Tilføj</span></button>
</div>
</div>
</form>


<?php } }?>
</div>
</div>
</body>
</html>