Podcast PHP System
====================

Made specifically for [UngKirke](http://ungkirke.com/).
Can be customized to suit your needs, it is meant for podcast management via a JSON file containing the information.

Made by: Simon Klit